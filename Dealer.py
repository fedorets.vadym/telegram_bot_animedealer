import telebot
import conf
import req #Сайт поставил защиту от ботов
import random

from threading import Timer
from telebot import types

bot = telebot.TeleBot(conf.token)

clients_adm = []
reply_queue = []
AdminChatID = -308831132
wait = []
answer = {}
ban_list = []
wait_for_ban_adm = []
wait_for_unban_adm = []
unique_users = []

def ads():
    for it in unique_users:
        try:
            keyboard = types.InlineKeyboardMarkup(row_width = 1)
            but = types.InlineKeyboardButton("Перейти", url = "https://instagram.com/_ani_mem_?igshid=3cwve84i6d27")
            keyboard.add(but)
            bot.send_message(it, "Любишь годные аниме мемы? Подписывайся на ахуенный инстаграм аккаунт как раз для тебя 😉", reply_markup = keyboard)
        except Exception:
            pass
    ntimer()

def share():
    for it in unique_users:
        try:
            bot.send_message(it, "🍪Поделись нашим ботом, помоги его развитию🍪")
        except Exception:
            pass
    timer()	

def timer():
	t = Timer(43200.0, ads)
	t.start()

def ntimer():
	t = Timer(43200.0, share)
	t.start()

timer()

def send_an(message):
	try:
		anime = req.anime()
		janeres = ""
		if float(anime[1]) < 7.50:
			send_an(message)
		else:
			for it in anime[5]:
				janeres += "\n    * " + it
			bot.send_message(message.chat.id, "Название : {0}\nРейтинг : {1}\nДата выхода : {2}, {3}\nВозрастной рейтинг : {4}\nСтатус : {5}\nЖанры :{6}".format(anime[0],anime[1],anime[3],anime[7].lower(),anime[8].split("\n")[0],anime[6], janeres))
			idi = bot.send_message(message.chat.id, "Высылаю вам картинку...").message_id
			bot.send_photo(message.chat.id, anime[2])
			keyboard = types.InlineKeyboardMarkup(row_width=2)
			button1 = types.InlineKeyboardButton(text = "Смотреть",url = anime[4])
			bot.delete_message(message.chat.id, idi)
			keyboard.add(button1)
			bot.send_message(message.chat.id, "{0}".format(("Приятного просмотра!🙃", "Насладись этим🙃", "Держи печеньку🍪", "Удачи😋")[random.randint(0,3)]), reply_markup = keyboard)
	except Exception:
		send_an(message)

@bot.message_handler(commands=['buttons'])
def reset_but(message):
	keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
	button1 = types.KeyboardButton("Получить аниме")
	button2 = types.KeyboardButton("/contact")
	keyboard.add(button1, button2)
	bot.send_message(message.chat.id, "А вот и кнопки🙃", reply_markup = keyboard)

@bot.message_handler(commands=['users'])
def users(message):
	bot.send_message(message.chat.id, "Количество уникальных пользователей : {0}".format(len(unique_users)))

@bot.message_handler(commands=['start'])
def welc(message):
	keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
	button1 = types.KeyboardButton("Получить аниме")
	button2 = types.KeyboardButton("/contact")
	keyboard.add(button1, button2)
	if message.chat.id != AdminChatID : 
		bot.send_message(message.chat.id, "Привет, <b>{0.first_name}</b>!\nМое имя - <b>{1.first_name}</b>...\nТы можешь получить аниме с помощью щелчка по кнопке!\nЕсли у вас пропали кнопки, воспользуйтесь командой\n\'/buttons\'  (Без ковычек)\nСвязь с администрацией - /contact".format(message.from_user, bot.get_me()), parse_mode='html', reply_markup = keyboard)

@bot.message_handler(commands=['ban'])
def ban_user(message):
	wait_for_ban_adm.append(str(message.from_user.id))

@bot.message_handler(commands=['unban'])
def ban_user(message):
	wait_for_unban_adm.append(str(message.from_user.id))

@bot.message_handler(commands=['contact'])
def admin_call(message):
	if str(message.chat.id) in ban_list:
		bot.send_message(message.chat.id, "Вы лишены данной возможности")
	else:	
		clients_adm.append(message.chat.id)
		bot.send_message(message.chat.id, "Напишите ваше сообщение, я его передам администрации и в скорем времени с вами свяжутся!🍪\nДля отмены /cancel.")

@bot.message_handler(commands=['cancel'])
def cancel_call(message):
	if message.from_user.id in clients_adm:
		bot.send_message(message.chat.id, "❌ Отменено ❌")
		clients_adm.remove(message.from_user.id)

@bot.message_handler(commands=['admin'])
def admin_menu(message):
	if message.chat.id == AdminChatID:
		bot.send_message(AdminChatID, "1. /reply\n2. /ban\n3. /unban")

@bot.message_handler(commands=['reply'])
def reply_mes(message):
	if message.chat.id == AdminChatID:
		bot.send_message(AdminChatID, "{0}, укажите номер курируемого чата".format(message.from_user.first_name))
		wait.append(message.from_user.id)

@bot.message_handler(content_types=['text'])
def ans(message):
	if message.from_user.id not in unique_users:
		unique_users.append(message.from_user.id)
	try:
		if message.chat.id != AdminChatID:
			if message.text == "Получить аниме":
				send_an(message)
			else:
				if message.chat.id not in clients_adm:
					bot.send_message(message.chat.id, "Не понимаю вас, но всё же вы можете получить аниме по щелчку кнопки!🤗")
				else:
					bot.send_message(AdminChatID, "Новый запрос\nНомер чата {0}\nСообщение : {1}\nUsername : {2}".format(message.chat.id, message.text, message.from_user.username))
					clients_adm.remove(message.chat.id)
					bot.send_message(message.chat.id, "Сообщение успешно отправлено, ждите ответа в ближайшее время!")
					reply_queue.append(str(message.chat.id))
		else:
			if message.from_user.id in answer.keys():
				bot.send_message(answer[message.from_user.id], "<b>От администратора {0}:</b>\n{1}".format(message.from_user.first_name, message.text), parse_mode = 'html')
				answer.pop(message.from_user.id)
			if message.from_user.id in wait:
				if message.text in reply_queue:
					bot.send_message(AdminChatID, "{0}, жду сообщение".format(message.from_user.first_name))
					answer[message.from_user.id] = message.text
					wait.remove(message.from_user.id)
					reply_queue.remove(message.text)
				else:
					bot.send_message(AdminChatID, "Данный чат не нуждается в поддержке")
					print(message.text)
					print(reply_queue)
					print(message.text in reply_queue)
			if str(message.from_user.id) in wait_for_ban_adm:
				ban_list.append(message.text)
				bot.send_message(AdminChatID, "Пользователь лишен возможности писать администрации!")
				wait_for_ban_adm.remove(str(message.from_user.id))
			if str(message.from_user.id) in wait_for_unban_adm:
				ban_list.remove(message.text)
				bot.send_message(AdminChatID, "Пользователю вернули возможность писать администрации!")
				wait_for_unban_adm.remove(str(message.from_user.id))
	except Exception: 
			bot.send_message(message.chat.id, "Произошла ошибка! Повторите попытку😭")
			print(Exception)

if __name__ == '__main__':
	bot.infinity_polling()
