import requests

myUrl = "https://yummyanime.club/random"

def anime():
	try:
		r = requests.get(myUrl)
		work_zone = r.text.split("<div class=\"poster-block\">")[1].split("<div class=\"content-desc\">")[0]
		name = work_zone.split("<div class=\"rating-info\" title=\"")[1].split("\">")[0]
		rate = work_zone.split("<span class=\"main-rating\">")[1].split("<")[0]
		img = work_zone.split("<img src=\"")[1].split("\"")[0]
		year = work_zone.split("<li><span>Год: </span>")[1].split("<")[0]
		janre_zone = work_zone.split("<span class=\"genre\">Жанр:</span>")[1].split("</ul>")[0].split(">")
		final_jar = janre_zone[3::4]
		janrs = []
		mounth = work_zone.split("<li><span>Сезон:</span>")[1].split("</li>")[0]
		years = work_zone.split("<span>Возрастной рейтинг:</span>")[1].split("</li>")[0]
		status = work_zone.split("<span class=\"badge\">")[1].split("</span>")[0]
		for it in final_jar:
			a = it.split("<")[0]
			janrs.append(a)
		return (name, rate, "https://yummyanime.club/"+img, year, r.url, janrs, status, mounth, years)
	except Exception:
		anime()

anime()